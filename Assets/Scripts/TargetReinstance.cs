﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetReinstance : MonoBehaviour {

    public static TargetReinstance instanciate;
    public GameObject target;
    public int numberTargets;
    private int points;

    private void Start()
    {
        for (int i = 0; i < numberTargets; i++)
        {
            Invoke("InstanceTargets", 2f + 2f * i);
            target.name = "target_" + i;
        }
        Physics2D.IgnoreLayerCollision(8, 8);
    }

    void Awake()
    {
        if (TargetReinstance.instanciate == null)
        {
            TargetReinstance.instanciate = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    //---------Aparición controlada de objetivos------------///

    public void InstanceTargets()
    {
        Instantiate(target);
    }

    public void ReduceTargets()
    {

        numberTargets = numberTargets - 1;
        points = points + 1;
    }

    public int ReturnNumberTargets()
    {
        return numberTargets;
    }

    public int ReturnPoints()
    {
        return points;
    }
}
