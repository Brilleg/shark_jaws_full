﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {

    public static GameControl instanciate;
    private float playTime;

    void Awake()
    {
        if (GameControl.instanciate == null)
        {
            instanciate = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        ReduceTime();
    }

    public void LoadScene(string value)
    {
        SceneManager.LoadScene(value);
    }

    //---------Reiniciar Nivel-----------//
    public void RestartCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //-----Administrador del tiempo limite-----------//
    public void ReduceTime()
    {
        playTime -= Time.deltaTime;
    }

    public void SetPlayTime(float time)
    {
        playTime = time;
    }

    public float GetPlayTime()
    {
        return playTime;
    }

    //---------Cerrar juego-----------//
    public void QuitGame()
    {
        Application.Quit();
    }
}
