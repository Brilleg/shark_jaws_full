﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static Player instanciate;
    private SpriteRenderer playerSprite;
    private Vector3 positionLimit;
    public float speed = 0;
    public int lives;

    private void Start()
    {
        //Captura la referncia del SpriteRender para este gameobject
        playerSprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Movement_player();
        Flip_sprite();
    }

    void Awake()
    {
        if (Player.instanciate == null)
        {
            Player.instanciate = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    //----------Control del movimiento del jugador----------//

    void Movement_player()
    {
        transform.Translate(speed * Input.GetAxis("Horizontal") * Time.deltaTime, speed * Input.GetAxis("Vertical") * Time.deltaTime, 0f);

        positionLimit = Camera.main.WorldToViewportPoint(transform.position);
        positionLimit.x = Mathf.Clamp01(positionLimit.x);
        positionLimit.y = Mathf.Clamp01(positionLimit.y);
        transform.position = Camera.main.ViewportToWorldPoint(positionLimit);
    }

    //-------------Vidas del jugador------------//

    public void ReduceLives()
    {
        lives = lives - 1;
    }

    public int ReturnLives()
    {
        return lives;
    }

    //------------Controlador del "Flip" para el Sprite---------------//
    void Flip_sprite()
    {
        if (playerSprite != null)
        {
            //Si la flecha izquierda es precionada voltea el sprite en x
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                //Voltea a la izquierda el sprite
                playerSprite.flipX = true;
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                //Voltea a la derecha el sprite
                playerSprite.flipX = false;

            }
        }
    }
}
