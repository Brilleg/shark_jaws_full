﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    private SpriteRenderer targetSprite;
    private Vector3 position;
    public float speed;
    public float minX;
    public float minY;
    public float maxX;
    public float maxY;


    private void Start()
    {
        //Captura la referncia del SpriteRender para este gameobject
        targetSprite = GetComponent<SpriteRenderer>();
        position = Camera.main.WorldToViewportPoint(transform.position);
        position.x = Random.Range(minX, maxX);
        position.y = Random.Range(minY, maxY);
        transform.position = new Vector2(position.x, position.y);
    }

    void Update()
    {
        Movement_target();
        TargetPatrol();
    }

    //-------------Movimiento del objetivo-----------------//

    void Movement_target()
    {
        Physics2D.IgnoreLayerCollision(8, 9);

        //Movimentos random por la pantalla
        transform.position = Vector2.MoveTowards(transform.position, position, speed * Time.deltaTime);

        //Si la variable no es nula se puede hacer la referencia al sprite
        if (targetSprite != null)
        {
            //Si la flecha izquierda es precionada voltea el sprrite en x
            if (position.x > transform.position.x)
            {
                //Voltea a la izquierda el sprite
                targetSprite.flipX = true;
            }

            if (position.x < transform.position.x)
            {
                //Voltea a la derecha el sprite
                targetSprite.flipX = false;
            }
        }
    }

    void TargetPatrol()
    {
        //Random para la patrulla del pez en pantalla
        if (Vector2.Distance(transform.position, position) < 0.2f)
        {
            position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
        }
    }
}
