﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionsManager : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Colisión contra con el enemigo
        if (collision.gameObject.tag == "Enemy")
        {
            Player.instanciate.ReduceLives();
            Physics2D.IgnoreLayerCollision(9, 10);
            StartCoroutine(InmortalTime(1.5F));
            Debug.Log(Player.instanciate.ReturnLives());
        }

        //Colisión contra con el objetivo
        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            TargetReinstance.instanciate.ReduceTargets();
        }
    }

    IEnumerator InmortalTime(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Physics2D.IgnoreLayerCollision(9, 10, false);
    }
}
