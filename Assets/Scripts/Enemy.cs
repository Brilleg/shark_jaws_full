﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float findSpeed;
    public float patrolSpeed;
    private Transform player;
    private SpriteRenderer enemySprite;
    private Vector3 patrolPositions;
    public float minX;
    public float minY;
    public float maxX;
    public float maxY;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        enemySprite = GetComponent<SpriteRenderer>();
        patrolPositions = Camera.main.WorldToViewportPoint(transform.position);
    }

    void Update()
    {
        Movement_enemy();
        Patrol_enemy();
        Wrap_screen();
    }

    //------------Control de movimento del enemigo--------------//

    void Movement_enemy()
    {
        //Mide la distancia entre el jugador y el enemigo; definiendo así la acción de seguir o patrullar
        if (player != null)
        {
            //Inicia la persecución
            if (Vector2.Distance(transform.position, player.position) < 5)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, findSpeed * Time.deltaTime);

                if (player.position.x > transform.position.x)
                {
                    //Voltear a la Derecha
                    enemySprite.flipX = false;
                }
                else if (player.position.x < transform.position.x)
                {
                    //Voltear a la Izquierda
                    enemySprite.flipX = true;
                }
            }
            else
            {
                //Inicia el movimiento de la patrulla
                transform.position = Vector2.MoveTowards(transform.position, patrolPositions, patrolSpeed * Time.deltaTime);
                if (patrolPositions.x > transform.position.x)
                {
                    //Voltear a la Derecha
                    enemySprite.flipX = false;
                }
                else if (patrolPositions.x < transform.position.x)
                {
                    //Voltear a la Izquierda
                    enemySprite.flipX = true;
                }
            }
        }
    }

    void Patrol_enemy()
    {
        //Random para la patrulla del tiburón en pantalla
        if (Vector2.Distance(transform.position, patrolPositions) < 0.2f)
        {
            patrolPositions = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
        }

    }

    //---------------Control del "Wrap" del enemigo en pantalla--------------------//
    void Wrap_screen()
    {
        //Wrap de derecha a izquierda
        if (player != null)
        {
            if (player.position.x <= -7 && transform.position.x > 5)
            {
                //Voltear a la Derecha
                enemySprite.flipX = false;

                transform.position = Vector2.MoveTowards(transform.position, (transform.position) * 2, findSpeed * Time.deltaTime);
                if (transform.position.x > 8)
                {
                    if (player.position.y > 0)
                    {
                        transform.position = new Vector2(-9, Random.Range(0, 5));
                    }
                    if (player.position.y < 0)
                    {
                        transform.position = new Vector2(-9, Random.Range(0, -5));
                    }
                }
            }
        }

        //Wrap de izquierda a derecha
        if (player != null)
        {
            if (player.position.x >= 7 && transform.position.x < -5)
            {
                //Voltear a la Izquierda
                enemySprite.flipX = true;

                transform.position = Vector2.MoveTowards(transform.position, (transform.position) * 2, findSpeed * Time.deltaTime);
                if (transform.position.x < -8)
                {
                    if (player.position.y > 0)
                    {
                        transform.position = new Vector2(9, Random.Range(0, 5));
                    }
                    if (player.position.y < 0)
                    {
                        transform.position = new Vector2(9, Random.Range(0, -5));
                    }
                }
            }
        }
    }
}
