﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utilities : MonoBehaviour {

    public GameObject creditsPanel;
    public GameObject pausePanel;
    public GameObject dataPanel;
    public GameObject countdownPanel;
    public GameObject winPanel;
    public GameObject losePanel;
    public GameObject countDown;
    public Text txtPlayTime;
    public Text txtCountLives;
    public Text txtCountPoints;
    public float remainPlayTime;
    private bool isPauseActive;

    private void Start()
    {
        GameControl.instanciate.SetPlayTime(remainPlayTime);
        StartCoroutine("CountDown");
    }

    void Update()
    {
        if(txtCountLives != null && txtCountPoints != null && txtPlayTime != null) {
            txtCountLives.text = Mathf.RoundToInt(Player.instanciate.ReturnLives()).ToString();
            txtCountPoints.text = Mathf.RoundToInt(TargetReinstance.instanciate.ReturnPoints()).ToString();
            txtPlayTime.text = Mathf.RoundToInt(GameControl.instanciate.GetPlayTime()).ToString();
        }

        PauseGame();
        WinLevel();
        LoseLevel(Mathf.RoundToInt(GameControl.instanciate.GetPlayTime()));
    }

    //----------Panel de tiempo------------------//

    IEnumerator CountDown()
    {
        if (countdownPanel != null && countDown != null)
        {
            Time.timeScale = 0f;
            float pauseTime = Time.realtimeSinceStartup + 4f;
            while (Time.realtimeSinceStartup < pauseTime)
            {
                yield return 0;
            }
            countDown.SetActive(false);
            countdownPanel.SetActive(false);
            Time.timeScale = 1;
        }
    }

    //----------Panel de Pausa--------------------//

    public void PauseGame()
    {
        if (pausePanel != null && countdownPanel.activeSelf == false)
        {
            if (isPauseActive == false && Input.GetKeyDown(KeyCode.Space))
            {
                if (dataPanel != null)
                {
                    pausePanel.SetActive(true);
                    Time.timeScale = 0f;
                    isPauseActive = true;
                }
            }
            else if (isPauseActive == true && Input.GetKeyDown(KeyCode.Space))
            {
                if (dataPanel != null)
                {
                    pausePanel.SetActive(false);
                    Time.timeScale = 1f;
                    isPauseActive = false;
                }
            }
        }
    }
    
    //----------Panel de gane--------------------//

    public void WinLevel()
    {
        if (winPanel != null && TargetReinstance.instanciate.ReturnNumberTargets() == 0)
        {
            winPanel.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    //----------Panel de perdida--------------------//

    public void LoseLevel(float time)
    {
        if (losePanel != null )
        {
            if (time == 0 || Player.instanciate.ReturnLives() == 0)
            {
                losePanel.SetActive(true);
                Time.timeScale = 0f;
            }
        }
    }
    //----------Panel de creditos----------------//
    public void SeeCredits()
    {
        if (creditsPanel != null)
        {
            creditsPanel.SetActive(true);

        }
        else
        {
            Debug.LogError("Verificar si el panel de créditos se encuntra en la escena");
        }
    }

    public void HideCredits()
    {
        if (creditsPanel != null)
        {
            creditsPanel.SetActive(false);

        }
        else
        {
            Debug.LogError("Verificar si el panel de créditos se encuntra en la escena");
        }
    }
}
